package com.Student.Management.Repository;

import com.Student.Management.Entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student , Integer> {




}
