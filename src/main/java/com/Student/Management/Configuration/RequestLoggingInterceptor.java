//package com.Student.Management.Configuration;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//
//@Component
//public class RequestLoggingInterceptor implements HandlerInterceptor {
//
//    private static final Logger logger = LoggerFactory.getLogger(RequestLoggingInterceptor.class);
//
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        logger.info("Request Host: {}", request.getRemoteHost());
//        logger.info("Request URL: {}", request.getRequestURL());
//        logger.info("Request Method: {}", request.getMethod());
//        logger.info("Request Headers: {}", request.getHeaderNames());
//        // Log other request details as needed
//        return true;
//    }
//}
//
//
//
//
