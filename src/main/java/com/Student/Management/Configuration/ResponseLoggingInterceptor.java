//package com.Student.Management.Configuration;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//
//@Component
//public class ResponseLoggingInterceptor implements HandlerInterceptor {
//
//    private static final Logger logger = LoggerFactory.getLogger(ResponseLoggingInterceptor.class);
//
//    @Override
//    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        logger.info("Response Host: {}", request.getRemoteHost());
//        logger.info("Response Status: {}", response.getStatus());
//        logger.info("Response Headers: {}", response.getHeaderNames());
//        // Log other response details as needed
//    }
//}
