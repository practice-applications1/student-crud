package com.Student.Management.Configuration;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AopConfig {
	
	@Before("execution(* com.Student.Management..*.*(..))")
	public void beforeExecution()
	{
		
		System.out.println("logging before execution");
	}
	
	@After("execution(* com.Student.Management.Service..*.*(..))")
	public void afterExecution()
	{
		
		System.out.println("logging After execution "+ getClass());
	}
	
	//@Around("execution(* com.Student.Management..*.*(..))")
//	public void aroundExecution()
//	{
		
	//	System.out.println("logging Around execution");
	//}

}
