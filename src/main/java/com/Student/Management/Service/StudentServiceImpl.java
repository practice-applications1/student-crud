package com.Student.Management.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Student.Management.Entity.Student;
import com.Student.Management.Repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Student createStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Optional<Student> getStudent(int id) {
        Optional<Student> byId = studentRepository.findById(id);
        return byId;
    }

    @Override
    public Student updateStudent(int id, Student student) {

        Student updatedStudent = studentRepository.findById(id).orElse(null);
        if (updatedStudent == null) {
            return null;
        } else {
            updatedStudent.setMobile(student.getMobile());
            updatedStudent.setAddress(student.getAddress());
            updatedStudent.setName(student.getName());


            return studentRepository.save(updatedStudent);

        }
    }

    @Override
    public String deleteStudent(int id) {


        studentRepository.deleteById(id);
        return "User has been deleted successFully";
    }

    @Override
    public List<Student> getAllStudents() {
        return (List<Student>) studentRepository.findAll();
    }

}
