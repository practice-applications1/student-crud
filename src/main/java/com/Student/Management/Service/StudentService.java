package com.Student.Management.Service;

import com.Student.Management.Entity.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {


    Student createStudent(Student student);

    Optional<Student> getStudent(int id);

    Student updateStudent(int id, Student student);

    String deleteStudent(int id);

    List<Student> getAllStudents();


}
