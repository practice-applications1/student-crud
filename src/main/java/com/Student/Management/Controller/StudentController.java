package com.Student.Management.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.Student.Management.Entity.Student;
import com.Student.Management.Service.StudentService;


import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;





    @PostMapping("/create")
    Student createNewStudent(@RequestBody Student student) {
        studentService.createStudent(student);
        return student;
    }

    @GetMapping(value = "/{id}" , produces = {"application/xml" , "application/json"} )
    Optional<Student> getStudentById(@PathVariable int id) {

        Optional<Student> student = studentService.getStudent(id);
        return student;

    }

    @DeleteMapping("/{id}")
    ResponseEntity<String> deleteStudentById(@PathVariable int id) {
        Optional<Student> student = studentService.getStudent(id);
        if (student.isPresent()) {
            return ResponseEntity.ok(studentService.deleteStudent(id));
        } else {
            return ResponseEntity.status(Objects.requireNonNull(HttpStatus.NOT_FOUND)).body("Student not found");
        }
    }

    @PutMapping("/{id}")
    String UpdateStudentByid(@RequestBody Student student, @PathVariable int id) {
        studentService.updateStudent(id, student);
        return "Student has been Modified";

    }

    @GetMapping("/getAll")
    List<Student> getAllStudents() {
        List<Student> allStudents = studentService.getAllStudents();
        return allStudents;

    }

}
